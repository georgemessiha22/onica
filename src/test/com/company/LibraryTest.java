package test.com.company;

import com.company.Book;
import com.company.Library;
import org.junit.jupiter.api.Order;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

class LibraryTest {
    /**
     * This test to basic functionality of requirements
     */
    private Library library;

    @org.junit.jupiter.api.BeforeEach
    void setUp() {
        File file = new File("library.csv");
        if (file.exists()) {
            file.delete();
        }
        try {
            FileWriter filew = new FileWriter(file, false);
            BufferedWriter writer = new BufferedWriter(filew);
            String[] books = {
                    "The Hobbit,George,this books description [1] The Hobbit\n",
                    "Lord of the Rings,adam,this books description [2] Lord of the Rings\n",
                    "Snow White and the Seven Dwarfs,work,this books description [3] Snow White and the Seven Dwarfs\n",
                    "Moby Dick,book,this books description [4] Moby Dick\n",
                    "Snow Crash,Lord,this books description [5] Snow Crash\n"};
            for (String book : books) {
                writer.write(book);
            }
            writer.close();
            filew.close();
        } catch (IOException e) {
            System.out.println(Arrays.toString(e.getStackTrace()));
        }
        this.library = new Library();
    }

    @org.junit.jupiter.api.Test
    @Order(1)
    void setBooks() {
        Book.resetCount();
        ArrayList<Book> books = new ArrayList<Book>();
        books.add(new Book("The Hobbit",
                "George", "this books description [1] The Hobbit"));
        books.add(new Book("Lord of the Rings",
                "adam", "this books description [2] Lord of the Rings"));

        Library l = new Library();
        l.setBooks(books);
        assert l.getBooks().size() == 2;
    }

    @org.junit.jupiter.api.Test
    @Order(2)
    void addBook() {
        assert this.library.getBooks().size() == 5;
        this.library.addBook(new Book("trial", "no one", "description"));
        assert this.library.getBooks().size() == 6;
    }

    @org.junit.jupiter.api.Test
    @Order(3)
    void removeBook() {
        this.library.removeBookById(1);
        assert this.library.getBooks().size() == 4;
    }

    @org.junit.jupiter.api.Test
    @Order(4)
    void searchBooks() {
        ArrayList<Book> books = this.library.searchBooks("book");
        assert books.size() == 5;
        books = this.library.searchBooks("lord");
        assert books.size() == 2;
    }
}