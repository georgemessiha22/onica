package com.company;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;


public class Library {
    private final String[] options = {
            "1) View all books",
            "2) Add a book",
            "3) Edit a book",
            "4) Search for a book.",
            "5) Delete book",
            "6) exit"
    };

    private List<Book> books;

    public Library() {
        this.books = new ArrayList<Book>();
        this.read_file();
    }

    public ArrayList<Book> getBooks() {
        return (ArrayList<Book>) books;
    }

    public void setBooks(ArrayList<Book> books) {
        this.books = books;
        this.update_file();
    }

    public void addBook(Book book) {
        this.books.add(book);
        this.update_file();
    }

    public void removeBookById(int id) {
        Book book = this.findBookById(id);
        if (book != null) {
            this.books.remove(book);
        }
        this.update_file();
    }

    private Book findBookById(int id) {
        for (Book book : this.books) {
            if (book.getId() == id) {
                return book;
            }
        }
        return null;
    }

    public ArrayList<Book> searchBooks(String searchKeyword) {
        ArrayList<Book> books = new ArrayList<Book>();

        for (Book book : this.books) {
            if (book.contains(searchKeyword)) {
                books.add(book);
            }
        }

        return books;
    }

    private void read_file() {
        File file = new File("library.csv");

        try {
            if (file.createNewFile()) {

                System.out.println("DB has been created.");
            } else {

                System.out.println("DB already exists.");
            }
            BufferedReader br = new BufferedReader(new FileReader(file));

            String st;
            Book.resetCount();
            while ((st = br.readLine()) != null) {
                this.books.add(new Book(st.split(",")[0], st.split(",")[1], st.split(",")[2]));
            }
        } catch (IOException e) {
            System.out.println(Arrays.toString(e.getStackTrace()));
        }
    }

    private void update_file() {
        File file = new File("library.csv");
        if (file.exists()) {
            file.delete();
        }
        try {
            FileWriter filew = new FileWriter(file, false);
            BufferedWriter writer = new BufferedWriter(filew);
            for (Book book : this.books) {
                writer.write(book.printBook());
                writer.write('\n');
            }
            writer.close();
            filew.close();
        } catch (IOException e) {
            System.out.println(Arrays.toString(e.getStackTrace()));
        }
    }

    private String displayBooks() {
        return this.displayBooks(null);
    }

    private String displayBooks(ArrayList<Book> books) {
        StringBuilder result = new StringBuilder();
        if (books == null) {
            books = (ArrayList<Book>) this.books;
        }
        for (Book book : books) {
            result.append('\t').append(book.displayBookSummary());
        }

        return result.toString();
    }


    /**
     * below methods are helper methods for display.
     */
    private void view_all_books() {
        System.out.println("\n==== View Books ====");
        System.out.println(this.displayBooks());
    }

    private void add_book(Scanner sc) {
        System.out.println("\n==== Add Book ====");
        System.out.println("Please enter the following information: ");

        Book book = new Book();
        System.out.print("\t\tTitle: ");
        book.setTitle(sc.next());
        System.out.print("\t\tAuthor: ");
        book.setAuthor(sc.next());
        System.out.print("\t\tDescription: ");
        book.setDescription(sc.next());
        this.addBook(book);
        System.out.println("Book [" + book.getId() + "] saved.");
    }

    private void edit_book(Scanner sc) {
        System.out.println("\n==== Edit Book ====\n");
        System.out.println(this.displayBooks());
        System.out.println("Enter the book ID of the book you want to edit; to return press <Enter>.");
        System.out.print("Book ID: ");
        String id_ = sc.nextLine();
        if (id_ == null || id_.length() == 0) {
            return;
        }
        int id = Integer.parseInt(id_);
        Book book = this.findBookById(id);
        if (book == null) {
            System.out.println("Invalid input.");
            return;
        }
        System.out.print("\nTitle [" + book.getTitle() + "]: ");
        book.setTitle(sc.nextLine());
        System.out.print("\nAuthor [" + book.getAuthor() + "]: ");
        book.setAuthor(sc.nextLine());
        System.out.print("\nDescription [" + book.getDescription() + "]: ");
        book.setDescription(sc.nextLine());
        this.update_file();
    }

    private void search_book(Scanner sc) {
        System.out.println("\n==== Search Book ====\n");
        System.out.println("Type in one or more keywords to search for; to return press <Enter>");
        System.out.print("Search: ");
        String searchKeyword = sc.nextLine();
        if (searchKeyword == null || searchKeyword.length() == 0) {
            return;
        }
        System.out.println(displayBooks(this.searchBooks(searchKeyword)));
        System.out.print("Book ID: ");
        String id_ = sc.nextLine();
        if (id_ == null || id_.length() == 0) {
            return;
        }
        int id = Integer.parseInt(id_);
        Book book = this.findBookById(id);
        if (book == null) {
            System.out.println("Invalid input.");
            return;
        }
        System.out.println(book.displayBook());
    }

    private void delete_book(Scanner sc) {
        System.out.println("\n==== Delete Book ====\n");
        System.out.println(this.displayBooks());
        System.out.println("Enter the book ID of the book you want to edit; to return press <Enter>.");
        System.out.print("Book ID: ");
        String id_ = sc.nextLine();
        if (id_ == null || id_.length() == 0) {
            return;
        }
        int id = Integer.parseInt(id_);
        Book book = this.findBookById(id);
        if (book == null) {
            System.out.println("Invalid input.");
            return;
        }
        System.out.println(book.displayBook());
        System.out.println("Please Confirm[N/y]: ");
        String answer = sc.nextLine();
        if (answer != null && answer.equalsIgnoreCase("y")) {
            this.removeBookById(id);
            System.out.println("Book [" + book.getId() + "] has been removed.");
        }
    }

    void play() {
        boolean play = true;
        Scanner sc = new Scanner(System.in);
        while (play) {
            System.out.println("\n=== Book Manager ===");
            for (String option : this.options) {
                System.out.println('\t' + option);
            }
            System.out.print("Choose Option [1-5]: ");
            String input_ = sc.nextLine();
            if (input_ == null || input_.length() == 0) {
                continue;
            }
            int input = Integer.parseInt(input_);
            switch (input) {
                case 1:
                    view_all_books();
                    break;
                case 2:
                    add_book(sc);
                    break;
                case 3:
                    edit_book(sc);
                    break;
                case 4:
                    search_book(sc);
                    break;
                case 5:
                    delete_book(sc);
                    break;
                case 6:
                    play = false;
                    System.out.println("GoodBye!");
                    break;
                default:
                    System.out.println("Invalid input.");
            }
        }
        sc.close();
    }
}
