package com.company;

public class Book {
    private static int counter = 1;
    private int id;
    private String title;
    private String author;
    private String description;

    public Book() {
        this.id = Book.counter;
        Book.counter++;
    }

    Book(String title) {
        this.id = Book.counter;
        Book.counter++;
        this.title = title;
    }

    public Book(String title, String author, String description) {
        this.id = Book.counter;
        Book.counter++;
        this.title = title;
        this.author = author;
        this.description = description;
    }

    public static void resetCount() {
        Book.counter = 1;
    }

    int getId() {
        return id;
    }

    String getTitle() {
        return title;
    }

    void setTitle(String title) {
        if (title == null || title.length() == 0) {
            return;
        }
        this.title = title;
    }

    String getAuthor() {
        return author;
    }

    void setAuthor(String author) {
        if (author == null || author.length() == 0) {
            return;
        }
        this.author = author;
    }

    String getDescription() {
        return description;
    }

    void setDescription(String description) {
        if (description == null || description.length() == 0) {
            return;
        }
        this.description = description;
    }

    boolean contains(String searchKeyword) {
        return this.title.toLowerCase().contains(searchKeyword.toLowerCase()) ||
                this.description.toLowerCase().contains(searchKeyword.toLowerCase()) ||
                this.author.toLowerCase().contains(searchKeyword.toLowerCase());
    }

    public String displayBookSummary() {
        return "[" + this.id + "] " + this.title + '\n';
    }

    public String displayBook() {
        return "[" + this.id + "]: Title: " + this.title + "\n\t Author: " + this.author + "\n\t Description: " + this.description;
    }

    public String printBook() {
        return this.title + "," + this.author + "," + this.description;
    }
}
